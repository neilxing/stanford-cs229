# Stanford CS229: Machine Learning

### Course Description
This course provides a broad introduction to machine learning and statistical pattern recognition. Topics include: supervised learning (generative/discriminative learning, parametric/non-parametric learning, neural networks, support vector machines); unsupervised learning (clustering, dimensionality reduction, kernel methods); learning theory (bias/variance tradeoffs, practical advice); reinforcement learning and adaptive control. The course will also discuss recent applications of machine learning, such as to robotic control, data mining, autonomous navigation, bioinformatics, speech recognition, and text and web data processing.

### Syllabus and Course Schedule
Time and Location: Monday, Wednesday 9:30am-10:50am, NVIDIA Auditorium.    
Class Videos: Current quarter's class videos are available [here](https://www.bilibili.com/video/av43634683) for everyone.


<div class="container">
<table id="schedule" class="table table-bordered no-more-tables">
  <thead class="active" style="background-color:#f9f9f9">
    <th>Event</th><th>Date</th><th>Description</th><th>Materials and Assignments</th>
  </thead>

  <tbody>
  <!--<tr>
    <td colspan="4" style="text-align:center; vertical-align:middle;background-color:#fffde7">
      <strong>Introduction</strong> (1 class)
    </td>
  </tr>-->
  <tr>
    <td>Lecture&nbsp;1</td>
    <td> 9/23 </td>
    <td>
      Introduction and Basic Concepts
    </td>
    <td>
    </td>
  </tr>
  <!--<tr>
    <td colspan="4" style="text-align:center; vertical-align:middle;background-color:#fffde7">
      <strong>Supervised learning</strong> (6 classes)
    </td>
  </tr>-->
  <tr>
    <td>Lecture&nbsp;2</td>
    <td>9/25</td>
    <td>Supervised Learning Setup. Linear Regression.
    </td>
    <td>
      <strong>Class Notes</strong>
      <ul>
      <li>Supervised Learning, Discriminative Algorithms <!-- [<a href="https://gitee.com/white-cloud-temple/stanford-cs229/raw/master/notes/cs229-notes1.ps">ps</a>] --> [<a href="https://gitee.com/white-cloud-temple/stanford-cs229/raw/master/notes/cs229-notes1.pdf">pdf</a>] </li>
      </ul>
    </td>
  </tr>
  <tr style="text-align:center; vertical-align:middle;background-color:#FFF2F2">
    <td>Assignment</td>
    <td>9/26</td>
    <td colspan="3" style="text-align:center; vertical-align:middle;">
      <strong>Problem Set 0</strong> <a href="https://piazza.com/class/k0s9q710pjn29t?cid=43">[link]</a>. Due Wednesday, Oct 2 at 11:59pm
    </td>
  </tr>
  <tr>
    <td>Section 1</td>
    <td>9/28</td>
    <td>
      <strong>Friday Lecture</strong>: Linear Algebra. 
    </td>
    <td>
      <strong>Notes</strong>
      <ul>
      <li>Linear Algebra Review and Reference [<a href="http://cs229.stanford.edu/section/cs229-linalg.pdf">pdf</a>]</li>
      <li>Linear Algebra, Multivariable Calculus, <br> and Modern Applications <br> (Stanford Math 51 course text) [<a href="https://web.stanford.edu/class/math51/stanford/math51book.pdf">pdf</a>]</li> 
      </ul>
    </td>
  </tr>
  <tr>
    <td>Lecture&nbsp;3</td>
    <td>9/30</td>
    <td rowspan="2">
      Weighted Least Squares. Logistic Regression. Netwon's Method <br>
      Perceptron. Exponential Family. Generalized Linear Models.
    </td>
    <td rowspan="2">
        <strong>Class Notes</strong>
        <ul>
        <li>Generative Algorithms [<a href="https://gitee.com/white-cloud-temple/stanford-cs229/raw/master/notes/cs229-notes2.pdf">pdf</a>] </li>
        </ul>
    </td>
  </tr>
  <tr>
    <td>Lecture&nbsp;4</td>
    <td>10/2</td>
  </tr>

  <tr style="text-align:center; vertical-align:middle;background-color:#FFF2F2">
    <td>Assignment</td>
    <td>10/2</td>
    <td colspan="3" style="text-align:center; vertical-align:middle;">
      <strong>Problem Set 1</strong> <a href="https://piazza.com/class/k0s9q710pjn29t?cid=149">[link]</a>. Due Wednesday, Oct 16 at 11:59pm
    </td>
  </tr>

  <tr>
    <td>Section 2</td>
    <td>10/4</td>
    <td>
      <strong>Friday Lecture</strong>: Probability <!-- [<a href="http://cs229.stanford.edu/section/cs229-prob.pdf">Notes</a>][<a href="http://cs229.stanford.edu/section/cs229-prob-slide.pdf">Slides</a>] -->
    </td>
    <td>
      <strong>Notes</strong>
      <ul>
      <li>Probability Theory Review [<a href="http://cs229.stanford.edu/section/cs229-prob.pdf">pdf</a>] </li>
      <li>The Multivariate Gaussian Distribution [<a href="http://cs229.stanford.edu/section/gaussians.pdf">pdf</a>] </li>
      <li>More on Gaussian Distribution [<a href="http://cs229.stanford.edu/section/more_on_gaussians.pdf">pdf</a>] </li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>Lecture&nbsp;5</td>
    <td>10/7</td>
    <td>
      Gaussian Discriminant Analysis. Naive Bayes.
    </td>
    <td>
    </td>
  </tr>
  <tr>
    <td>Lecture&nbsp;6</td>
    <td>10/9</td>
    <td>
      Laplace Smoothing. Support Vector Machines. <br>
    </td>
    <td>
      <strong>Class Notes</strong>
      <ul>
      <li>Support Vector Machines [<a href="https://gitee.com/white-cloud-temple/stanford-cs229/raw/master/notes/cs229-notes3.pdf">pdf</a>] </li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>Section 3</td>
    <td>10/11</td>
    <td>
      <strong>Friday Lecture</strong>: Python and Numpy<!-- <a href="https://d1b10bmlvqabco.cloudfront.net/attach/jkbylqx4kcp1h3/jm8g1m67da14eq/jn7zkozyyol7/CS229_Python_Tutorial.pdf">[slides]</a> <Vectorization[<a href="http://cs229.stanford.edu/section/vec_demo/Vectorization_Section.pdf">Slides</a>][<a href="http://cs229.stanford.edu/section/vec_demo/knn.py">kNN</a>][<a href="http://cs229.stanford.edu/section/vec_demo/lr.ipynb">Logistic Regression</a>][<a href="http://cs229.stanford.edu/section/vec_demo/sr.ipynb">Softmax Regression</a>][<a href="http://cs229.stanford.edu/section/vec_demo/images.csv">images</a>][<a href="http://cs229.stanford.edu/section/vec_demo/labels.csv">labels</a> -->
    </td>
    <td>
    <strong> Notes</strong>
    <ul>
    <li> Python Tutorial [<a href="http://cs229.stanford.edu/section/cs229_python_friday.pptx">pptx</a>] [<a href="http://cs229.stanford.edu/section/python_tutorial.zip">code</a>]</li>
    </ul>
    </td>
  </tr>
  <tr>
    <td>Lecture&nbsp;7</td>
    <td>10/14</td>
    <td colspan="2">
      Support Vector Machines. Kernels.
    </td>
  </tr>
<!--   <tr>
    <td>Lecture&nbsp;2</td>
    <td> 9/27 </td>
    <td rowspan="6">
      <strong>Supervised learning</strong> (5 classes)
                 <ol>
                  <li>Supervised learning setup.  LMS.</li>
                  <li>Logistic regression.  Perceptron.  Exponential family.  </li>
                  <li>Generative learning algorithms. Gaussian discriminant analysis.  Naive Bayes. </li>
                  <li>Support vector machines.  </li>
                  <li>Model selection and feature selection. </li>
                  <li>Evaluating and debugging learning algorithms. </li>
                </ol>
            </td>
    <td rowspan="6">
      <strong>Class Notes</strong>
      <ul>
      <li>Generative Algorithms [<a href="http://cs229.stanford.edu/notes/cs229-notes2.ps">ps</a>] [<a href="http://cs229.stanford.edu/notes/cs229-notes2.pdf">pdf</a>] </li>
      <li>Support Vector Machines [<a href="http://cs229.stanford.edu/notes/cs229-notes3.ps">ps</a>] [<a href="http://cs229.stanford.edu/notes/cs229-notes3.pdf">pdf</a>] </li>
      </ul>

      <strong>Problem Set 1</strong> <a href="ps/ps1/ps1.pdf">[pdf]</a>. Out 10/4. Due 10/18. <a href="gradescope.html">Submission instructions</a>.<br>
      <strong>Discussion Section: Probability</strong> [<a href="http://cs229.stanford.edu/section/cs229-prob.pdf">Notes</a>][<a href="http://cs229.stanford.edu/section/cs229-prob-slide.pdf">Slides</a>]<br>
      <strong>Discussion Section: Vectorization</strong> [<a href="http://cs229.stanford.edu/section/vec_demo/Vectorization_Section.pdf">Slides</a>][<a href="http://cs229.stanford.edu/section/vec_demo/knn.py">kNN</a>][<a href="http://cs229.stanford.edu/section/vec_demo/lr.ipynb">Logistic Regression</a>][<a href="http://cs229.stanford.edu/section/vec_demo/sr.ipynb">Softmax Regression</a>]<br>
    </td>
  </tr>
  <tr>
    <td>
      Section
    </td>
    <td> 9/29 </td>
  </tr>
  <tr>
    <td>Lecture&nbsp;3</td>
    <td> 10/2 </td>
  </tr>

  <tr>
    <td>Lecture&nbsp;4</td>
    <td> 10/4 </td>
  </tr>

  <tr>
    <td>Lecture&nbsp;5</td>
    <td> 10/9 </td>
  </tr>

  <tr>
    <td>Lecture&nbsp;6</td>
    <td> 10/11 </td>

  </tr> -->
  <!--<tr>
    <td colspan="4" style="text-align:center; vertical-align:middle;background-color:#fffde7">
      <strong>Learning theory </strong> (2 classes)
    </td>
  </tr>-->
  <tr>
    <td>Lecture&nbsp;8</td>
    <td> 10/16 </td>
    <td>Neural Networks - 1
    </td>
    <td>
    <strong>Class Notes</strong>
    <ul>
      <!-- <li>Bias/variance tradeoff and error analysis[<a href="http://cs229.stanford.edu/section/error-analysis.pdf">pdf</a>]</li> -->
      <li>Deep Learning [<a href="https://gitee.com/white-cloud-temple/stanford-cs229/raw/master/notes/cs229-notes-deep_learning.pdf">pdf</a>] </li>
      <li>Backpropagation [<a href="http://cs229.stanford.edu/notes/cs229-notes-backprop.pdf">pdf</a>] </li>
    </ul>
    </td>
  </tr>

  <tr style="text-align:center; vertical-align:middle;background-color:#FFF2F2">
    <td>Assignment</td>
    <td>10/16</td>
    <td colspan="3" style="text-align:center; vertical-align:middle;">
      <strong>Problem Set 2</strong> <a href="https://piazza.com/class/k0s9q710pjn29t?cid=428">[link]</a>. Due Wednesday, Oct 30 at 11:59pm
      <br>
    </td>
  </tr>

  <tr>
    <td>Section 4</td>
    <td>10/18</td>
    <td>
      <strong>Friday Lecture</strong>: Evaluation Metrics
    </td>
    <td>
      <strong>Notes</strong>
      <ul>
        <li>Evaluation Metrics [<a href="http://cs229.stanford.edu/section/evaluation_metrics.pdf">pdf</a>]</li>
      </ul>
    </td>
  </tr>
  <tr style="text-align:center; vertical-align:middle;background-color:#FFF2F2">
    <td>Project</td>
    <td> 10/18 </td>
     <td colspan="2" style="text-align:center; vertical-align:middle;"><strong>Project proposal</strong> due 10/18 at 11:59pm.</td>
  </tr>

  <tr>
    <td>Lecture&nbsp;9</td>
    <td> 10/21 </td>
    <td>
	    Neural Networks - 2
    </td>
  </tr>

  <tr>
    <td>Lecture&nbsp;10</td>
    <td>10/23</td>
    <td>
      Bias - Variance. Regularization. Feature / Model selection.
    </td>
    <td>
    <strong>Class Notes</strong>
    <ul>
      <li>Regularization and Model Selection [<a href="https://gitee.com/white-cloud-temple/stanford-cs229/raw/master/notes/cs229-notes5.pdf">pdf</a>] </li>
      </ul>
    </td>
  </tr>

  <tr>
    <td>Section 5</td>
    <td>10/25</td>
    <td>
      <strong>Friday Lecture</strong>: Deep Learning
    </td>
    <td>
    <strong>Notes</strong>
    <ul>
      <li>Deep Learning [<a href="http://cs229.stanford.edu/section/deep_learning.pdf">pdf</a>]
      </li>
    </ul>
    </td>
  </tr>

  <tr>
    <td>Lecture&nbsp;11</td>
    <td>10/28</td>
    <td>Practical Advice for ML projects.
      </td>
    <td>
    <strong>Class Notes</strong>
    <ul>
      <li>ML Advice [<a href="http://cs229.stanford.edu/materials/ML_Advice_Lecture-2019-Oct28.pdf">pdf</a>]</li>
      <!-- <li>Advice on applying machine learning [<a href="http://cs229.stanford.edu/materials/ML-advice.pdf">pdf</a>]</li> -->
    </ul>
    </td>
  </tr>
  <tr style="text-align:center; vertical-align:middle;background-color:#FFF2F2">
    <td>Assignment</td>
    <td>10/30</td>
    <td colspan="3" style="text-align:center; vertical-align:middle;">
      <strong>Problem Set 3</strong> <a href="https://piazza.com/class/k0s9q710pjn29t?cid=731">[link]</a>. Due Wednesday, Nov 13 at 11:59pm<br>
    </td>
  </tr>

  <tr>
    <td>Lecture&nbsp;12 </td>
    <td>10/30</td>
    <td>K-Means. GMM (non EM). Expectation Maximization.
    </td>
    <td>
      <strong>Class Notes</strong>
      <ul>
      <li>Unsupervised Learning, k-means clustering. [<a href="http://cs229.stanford.edu/notes/cs229-notes7a.pdf">pdf</a>]</li>
      <li>Mixture of Gaussians [<a href="http://cs229.stanford.edu/notes/cs229-notes7b.pdf">pdf</a>] </li>
      <li>The EM Algorithm [<a href="http://cs229.stanford.edu/notes/cs229-notes8.pdf">pdf</a>] </li>
      </ul>
    </td>
  </tr>

  <tr>
    <td>Section 6</td>
    <td>11/1</td>
    <td>
      <strong>Friday Lecture</strong>: Midterm Review
    </td>
    <td>
      <strong>Class Notes</strong>
      <ul>
      <li>Midterm review  [<a href="materials/midterm-review.pdf">pdf</a>]</li>
      </ul>
    </td>
  </tr>

  <tr>
    <td>Lecture&nbsp;13</td>
    <td>11/4</td>
    <td>Expectation Maximization. Factor Analysis.</td>
    <td>
      <strong>Class Notes</strong>
      <ul>
      <li>Factor Analysis [<a href="https://gitee.com/white-cloud-temple/stanford-cs229/raw/master/notes/cs229-notes9.pdf">pdf</a>]</li>
      </ul>
      </td>
    </td>
  </tr>

  <tr style="text-align:center; vertical-align:middle;background-color:#FFF2F2">
    <td>Midterm</td>
    <td>11/5</td>
    <td colspan="2">
      <span style="text-align: left;">The midterm details are posted <a href="https://piazza.com/class/k0s9q710pjn29t?cid=62">on Piazza</a>.</span>
    </td>
  </tr>

  <tr>
    <td>Lecture&nbsp;14</td>
    <td> 11/6 </td>
    <td>Principal and Independent Component Analysis.</td>
    <td>
      <strong>Class Notes</strong>
      <ul>
      <li>Principal Components Analysis [<a href="https://gitee.com/white-cloud-temple/stanford-cs229/raw/master/notes/cs229-notes10.pdf">pdf</a>] </li>
      <li>Independent Component Analysis [<a href="https://gitee.com/white-cloud-temple/stanford-cs229/raw/master/notes/cs229-notes11.pdf">pdf</a>] </li>
      </ul>
      </td>
    </td>
  </tr>

  <tr>
    <td>Section 7</td>
    <td>11/8</td>
    <td>
      <strong>Friday Lecture</strong>: Decision Trees. Boosting. Bagging.
    </td>
    <td>
      <strong>Class Notes</strong>
      <ul>
        <li>Decision trees [<a href="http://cs229.stanford.edu/notes/cs229-notes-dt.pdf">pdf</a>] </li>
        <li>Decision tree ipython demo [<a href="https://gitee.com/white-cloud-temple/stanford-cs229/raw/master/notes/ta_lecture/decision_tree_demo.ipynb">ipynb</a>] </li>
        <li>Boosting algorithms and weak learning [<a href="http://cs229.stanford.edu/extra-notes/boosting.pdf">pdf</a>] </li>
      </ul>
    </td>
  </tr>

  <tr>
    <td>Lecture&nbsp;15</td>
    <td> 11/11 </td>
    <td rowspan="2"> Weak Supervision</td>
    <td rowspan="2">
      <strong>Class Notes</strong>
      <ul>
      <li>Weak Supervision [<a href="https://gitee.com/white-cloud-temple/stanford-cs229/raw/master/notes/weak_supervision_slides.pdf">slides pdf</a>] </li>
      <li>Weak Supervision [<a href="https://gitee.com/white-cloud-temple/stanford-cs229/raw/master/notes/weak_supervision_notes.pdf">notes pdf</a>] </li>
      </ul>
      </td>
    </td>
  </tr>

  <tr>
    <td>Lecture&nbsp;16</td>
    <td> 11/13</td>
  </tr>

  <tr style="text-align:center; vertical-align:middle;background-color:#FFF2F2">
    <td>Assignment</td>
    <td>11/13</td>
    <td colspan="3" style="text-align:center; vertical-align:middle;">
      <strong>Problem Set 4</strong>
      <a href="https://piazza.com/class/k0s9q710pjn29t?cid=1032">[link]</a>
      . Due Wednesday, Dec 4 at 11:59pm<br>
    </td>
  </tr>

  <tr>
    <td>Section 8</td>
    <td>11/15</td>
    <td>
      <strong>Friday Lecture</strong>: On critiques of Machine Learning
    </td>
    <td>
      <strong>Class Notes</strong>
      <ul>
        <li>On critiques of ML [<a href="materials/critiques-ml-aut19.pdf">slides</a>] </li>
      </ul>
    </td>
  </tr>

  <tr style="text-align:center; vertical-align:middle;background-color:#FFF2F2">
    <td>Project</td>
    <td> 11/15 </td>
     <td colspan="2"><strong>Project milestones</strong> due 11/15 at 11:59pm.</td>
  </tr>

  <tr>
    <td>Lecture&nbsp;17 </td>
    <td> 11/18 </td>
    <td> Value Iteration and Policy Iteration </td>
    <td>
     <strong>Class Notes</strong>
     <ul>
      <li>Reinforcement Learning and Control [<a href="https://gitee.com/white-cloud-temple/stanford-cs229/raw/master/notes/cs229-notes12.pdf">pdf</a>]</li>
      </li> 
    </ul>
    </td>
  </tr>

  <tr>
    <td>Lecture&nbsp;18</td>
    <td> 11/20 </td>
    <td>Bias and Variance</td>
    <td>
     <strong>Class Notes</strong>
     <ul>
      <li>Bias / Variance [<a href="http://cs229.stanford.edu/summer2019/BiasVarianceAnalysis.pdf">pdf</a>]</li>
      </li> 
    </ul>
    </td>
  </tr>

  <tr>
    <td>Lecture&nbsp;19</td>
    <td> 12/2 </td>
    <td>Learning Theory</td>
    <td>
     <strong>Class Notes</strong>
     <ul>
      <li>Learning Theory [<a href="https://gitee.com/white-cloud-temple/stanford-cs229/raw/master/notes/cs229-notes4.pdf">pdf</a>]</li>
      </li> 
    </ul>
    </td>
  </tr>

  <tr>
    <td>Lecture&nbsp;20</td>
    <td> 12/4 </td>
    <td>Course wrap-up. Beyond CS229 Guest Lectures! Details <a href="https://piazza.com/class/k0s9q710pjn29t?cid=1150">[link]</a></td>
    <td></td>
  </tr>

  <tr style="text-align:center; vertical-align:middle;background-color:#FFF2F2">
    <td>Project</td>
    <td> 12/11 </td>
     <td colspan="2"> <strong>Poster submission</strong> deadline, due 12/11 at 11:59pm (no late days).</td>
  </tr>
  <tr style="text-align:center; vertical-align:middle;background-color:#FFF2F2">
    <td>Project</td>
    <td> 12/12 </td>
     <td colspan="2"> <strong>Poster presentations</strong> from 8:30-11:30am.  Venue and details to be announced.</td>
  </tr>

  <tr style="text-align:center; vertical-align:middle;background-color:#FFF2F2">
    <td>Project</td>
    <td> 12/13 </td>
     <td colspan="2"><strong>Project final report</strong> due 12/13 at 11:59pm (no late days).</td>
  </tr>

  <tr class="warning" id="opt">
    <td colspan="4">
    <b>Supplementary Notes</b>
    <ol>
      <li>Online Learning and the Perceptron Algorithm [<a href="https://gitee.com/white-cloud-temple/stanford-cs229/raw/master/notes/cs229-notes6.pdf">pdf</a>] </li>
      <li>Binary classification with +/-1 labels [<a href="http://cs229.stanford.edu/extra-notes/loss-functions.pdf">pdf</a>]</li>
      <!-- <li>Functional after implementing stump_booster.m in PS2. [<a href="http://cs229.stanford.edu/extra-notes/boosting_example.m">here</a>] </li> -->
      <li>The representer theorem [<a href="http://cs229.stanford.edu/extra-notes/representer-function.pdf">pdf</a>]</li>
      <li>Hoeffding's inequality [<a href="http://cs229.stanford.edu/extra-notes/hoeffding.pdf">pdf</a>] </li>
    </ol></td>
  </tr>

  <!--
  <tr class="alert">
    <td colspan="4">
    <b>Friday Lecture Notes</b>
    <ol>
      <li>Convex Optimization Overview, Part I [<a href="http://cs229.stanford.edu/section/cs229-cvxopt.ps">ps</a>] [<a href="http://cs229.stanford.edu/section/cs229-cvxopt.pdf">pdf</a>]</li>
      <li>Convex Optimization Overview, Part II [<a href="http://cs229.stanford.edu/section/cs229-cvxopt2.ps">ps</a>] [<a href="http://cs229.stanford.edu/section/cs229-cvxopt2.pdf">pdf</a>] </li>
      <li>Hidden Markov Models [<a href="http://cs229.stanford.edu/section/cs229-hmm.ps">ps</a>] [<a href="http://cs229.stanford.edu/section/cs229-hmm.pdf">pdf</a>] </li>
      <li>Gaussian Processes [<a href="http://cs229.stanford.edu/section/cs229-gaussian_processes.pdf">pdf</a>] </li>
    </ol></td>
  </tr>
  -->
  <tr>
    <td colspan="4">
      <b>Other Resources</b>
      <ol>
        <li>Advice on applying machine learning: Slides from Andrew's lecture on getting machine learning algorithms to work in practice can be found <a href="http://cs229.stanford.edu/materials/ML-advice.pdf">here</a>.<br></li>
        <li>Previous projects: A list of last year's final projects can be found <a href="http://cs229.stanford.edu/proj2017/index.html">here</a>.<br></li>
        <!--<li>Matlab resources: Here are a couple of Matlab tutorials that you might find helpful: <a href="http://www.math.ucsd.edu/~bdriver/21d-s99/matlab-primer.html">http://www.math.ucsd.edu/~bdriver/21d-s99/matlab-primer.html</a> and <a href="http://www.math.mtu.edu/~msgocken/intro/node1.html">http://www.math.mtu.edu/~msgocken/intro/node1.html</a>. For emacs users only: If you plan to run Matlab in emacs, here are <a href="http://cs229.stanford.edu/materials/matlab.el">matlab.el</a>, and a helpful <a href="http://cs229.stanford.edu/materials/emacs">.emac's</a> file.<br></li>
        <li>Octave resources: For a free alternative to Matlab, check out <a href="http://www.gnu.org/software/octave/">GNU Octave</a>. The official documentation is available <a href="http://www.gnu.org/software/octave/doc/interpreter/">here</a>. Some useful tutorials on Octave include <a href="http://en.wikibooks.org/wiki/Octave_Programming_Tutorial">http://en.wikibooks.org/wiki/Octave_Programming_Tutorial</a> and <a href="http://www-mdp.eng.cam.ac.uk/web/CD/engapps/octave/octavetut.pdf">http://www-mdp.eng.cam.ac.uk/web/CD/engapps/octave/octavetut.pdf</a> .<br></li>-->
        <li>Data: Here is the <a href="http://www.ics.uci.edu/~mlearn/MLRepository.html">UCI Machine learning repository</a>, which contains a large collection of standard datasets for testing learning algorithms. If you want to see examples of recent work in machine learning, start by taking a look at the conferences <a href="http://www.nips.cc/">NIPS</a>(all old NIPS papers are online) and ICML. Some other related conferences include UAI, AAAI, IJCAI.<br></li>
        <li>Viewing PostScript and PDF files: Depending on the computer you are using, you may be able to download a <a href="http://www.cs.wisc.edu/~ghost/">PostScript</a> viewer or <a href="http://www.adobe.com/products/acrobat/readstep2_allversions.html">PDF viewer</a> for it if you don't already have one.<br></li>
        <li><a href="https://stanford.edu/~shervine/teaching/cs-229/cheatsheet-supervised-learning">Machine learning study guides tailored to CS 229</a> by Afshine Amidi and Shervine Amidi.</li>
      </ol>
    </td>
  </tr>

</tbody></table>
</div>